# Vue.js And Vue Native Installations And Configurations

## Prerequisites

> Installed: Node.js ([Downloads](https://nodejs.org/en/download/current/))
>
> Installed: Visual Studio Code ([Download Visual Studio Code](https://code.visualstudio.com/Download))

## Vue.js ([Installation](https://cli.vuejs.org/guide/installation.html), [Using The GUI](https://cli.vuejs.org/guide/creating-a-project.html#using-the-gui  ) And [Vue UI Install](https://vuetifyjs.com/en/getting-started/quick-start/#vue-ui-install))

### Vue.js (Vue.js)

#### macOS Terminal Application (Vue.js)

`sudo npm i -g @vue/cli`

`nano ~/.vuerc`

> Enter: ([.vuerc](https://gitlab.com/Ezohr-InstallationsAndConfigurations/Vue.jsAndVueNativeInstallationsAndConfigurations/-/blob/master/.vuerc))

```JSON
{
  "useTaobaoRegistry": false,
  "presets": {
    "Complete 20200522 20200530": {
      "useConfigFiles": false,
      "plugins": {
        "@vue/cli-plugin-babel": {},
        "@vue/cli-plugin-typescript": {
          "classComponent": true,
          "useTsWithBabel": true
        },
        "@vue/cli-plugin-pwa": {},
        "@vue/cli-plugin-router": {
          "historyMode": true
        },
        "@vue/cli-plugin-vuex": {},
        "@vue/cli-plugin-eslint": {
          "config": "prettier",
          "lintOn": [
            "save"
          ]
        },
        "@vue/cli-plugin-unit-mocha": {},
        "@vue/cli-plugin-e2e-nightwatch": {}
      },
      "cssPreprocessor": "node-sass"
    }
  },
  "latestVersion": "4.4.1",
  "lastChecked": 1590842157766
}
```

> Enter: Control + x
>
> Enter: y

`vue ui`

#### Visual Studio Code Application (Vue.js Part 1)

> Edit: .gitignore
>
> Enter: ([.gitignore](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/Vue.jsAndVueNativeInstallationsAndConfigurations/-/blob/master/.gitignore))

```sh
# Excluding JavaScript And Map Files (Using TypeScript)
*.js
*.map
```

#### Visual Studio Code Application (Vue.js Part 2)

> Select: [Extensions]
>
> Search: Vetur (Pine Wu)
>
> Select: Install
>
> Search: Sass (Syler)
>
> Select: Install
>
> Search: language-stylus (sysoev)
>
> Select: Install
>
> Search: ESLint (Dirk Baeumer)
>
> Select: Install

***

## Vuetify

### macOS Safari Application (Vuetify)

> Select: Plugins
>
> Select: + Add Plugin
>
> Search: vuetify
>
> Select: vue-cli-plugin-vuetify
>
> Select: Install vue-cli-plugin-vuetify
>
> Select: Finish Installation
>
> Select: Continue

### Visual Studio Code Application (Vuetify)

> Edit: tsconfig.json
>
> Enter: ([tsconfig.json](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/Vue.jsAndVueNativeInstallationsAndConfigurations/-/blob/master/tsconfig.json))

```JSON
"types": [
  "vuetify"
]
```

***

## Electron

### macOS Safari Application (Electron)

> Select: Plugins
>
> Select: + Add Plugin
>
> Search: Electron
>
> Select: vue-cli-plugin-electron-builder
>
> Select: Install vue-cli-plugin-electron-builder
>
> Select: ^6.0.0 (Default)
>
> Select: [Add Tests With Spectron To Your Project]
>
> Select: Finish Installation
>
> Select: Continue

***

## Vue Native ([Setup With Vue Native CLI](https://vue-native.io/docs/installation.html#Setup-with-Vue-Native-CLI) And [For Expo Users](https://vue-native.io/docs/installation.html#For-Expo-users))

### macOS Terminal Application (Vue Native)

`sudo npm i -g vue-native-cli`

`sudo npm i -g expo-cli`

`vue-native init [Project]`
